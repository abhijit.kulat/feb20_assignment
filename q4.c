#include<stdio.h>
#include<stdlib.h>
#include<string.h>


typedef struct books{
	int id;
	char name[50];
	int price;
}book;

#define SIZE 10

void add_det(book d[]);
void srt_price(book d[]);
void srt_name(book d[]);
void display_all(book d[]);
int comparator(const void *p, const void *q);
void mergeSort(book arr[], int l, int r);
void merge(book arr[], int l, int m, int r);

int main()
{
	int ch = 1,i;
	char c;
	book arr[SIZE],temp1[SIZE],temp2[SIZE];
	while(ch != 0)
	{
		printf("\n 0. Exit\n1. Add Book \n2. Sort By Name \n3.Sort By price \n 4. Display All\n Enter Your Choics: ");
		scanf("%d",&ch);
		switch(ch)
		{
			case 1: add_det(arr);
					break;
			case 2: 
					for(i=0;i<SIZE;i++)
					{
						temp1[i] = arr[i];
					}
					srt_name(temp1);
					break;
			case 3: 
                      for(i=0;i<SIZE;i++)
                      {
                          temp2[i] = arr[i];
                      }

					srt_price(temp2);
					break;
			case 4: display_all(arr);
					break;
		}
	}

	return 0;
}

void add_det(book d[])
{
	int i;
	char c;
	for(i=0;i<SIZE;i++)
	{
		printf("\n Enter id :");
		scanf("%d",&d[i].id);
		scanf("%c",&c);
		printf("\n Enter name :");
		scanf("%s",d[i].name);
		printf("\n Enter Price :");
		scanf("%d",&d[i].price);
	}
}
void srt_price(book d[])
{
	mergeSort(d, 0, SIZE - 1); 
}
void srt_name(book d[])
{
	qsort(d,SIZE,sizeof(book),comparator);

	display_all(d);
}

void display_all(book d[])
{
	int i=0;
	for(i=0;i<SIZE;i++)
	{
		printf("\n Id: %d  \tName: %s  \t Price: %d",d[i].id,d[i].name,d[i].price);
	}
	display_all(d);
}

int comparator(const void *p, const void *q)
{
	book *a = (book *)p;
	book *b = (book *)q;

	return strcmp(a->name,b->name);
}

void merge(book arr[], int l, int m, int r)
{
	int i, j, k;
	int n1 = m - l + 1;
	int n2 =  r - m;

	book L[n1], R[n2];

	for (i = 0; i < n1; i++)
		L[i] = arr[l + i];
	for (j = 0; j < n2; j++)
		R[j] = arr[m + 1+ j];

	i = 0; // Initial index of first subarray
	j = 0; // Initial index of second subarray
	k = l; // Initial index of merged subarray
	while (i < n1 && j < n2)
	{
		if (L[i].price >= R[j].price)
		{
			arr[k] = L[i];
			i++;
		}
		else
		{
			arr[k] = R[j];
			j++;
		}
		k++;
	}

		while (i < n1)
		{
			arr[k] = L[i];
			i++;
			k++;
		}

	while (j < n2)
	{
		arr[k] = R[j];
		j++;
		k++;
	}
}

void mergeSort(book arr[], int l, int r)
{
	if (l < r)
	{
		int m = l+(r-l)/2;

		mergeSort(arr, l, m);
		mergeSort(arr, m+1, r);

		merge(arr, l, m, r);
	}
}
